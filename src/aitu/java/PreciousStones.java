package aitu.java;

public class PreciousStones extends Stones {
    PreciousStones(int id, String name, double price_per_carat, double weight){
        super(id, name, price_per_carat, weight, "precious");
    }
}
