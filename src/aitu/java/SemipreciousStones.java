package aitu.java;

public class SemipreciousStones extends Stones{
    SemipreciousStones(int id, String name, double price_per_carat, double weight){
        super(id, name, price_per_carat, weight, "semiprecious");
    }
}
